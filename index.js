'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();

app.use(cors());
app.use(bodyParser.json());

const restAPI = express.Router();

mongoose.connect('mongodb://localhost/test', {
	useMongoClient: true
});

const userSchema = new mongoose.Schema({
	name: {
		type: String,
		unique: true,
		required: true,
		dropDups: true
	}
});

const taskSchema = new mongoose.Schema({
	name: String,
	description: String,
	isOpened: {
		type: Boolean,
		default: true
	},
	date: {
		type: Date
	},
	user: String
});

const User = mongoose.model('User', userSchema);
const Task = mongoose.model('Task', taskSchema);

restAPI.get('/users/', (req, res) => {
	User.find().exec((err, docs) => {
		if (err) {
			throw err;
		} else {
			res.json(docs);
		}
	});
});

restAPI.post('/users/', (req, res) => {
	if (req.body.name) {
		const newUser = new User({
			name: req.body.name
		});

		newUser.save(err => {
			if (err) {
				res.json(err);
			} else {
				res.json(true);
			}
		});
	} else {
		res.status(400);
		res.send();
	}
});

restAPI.put('/users/:id', (req, res) => {
	if (req.body.name) {
		User.findOne({
			_id: new mongodb.ObjectId(req.params.id)
		}, (err, doc) => {
			if (err) {
				throw err;
			} else {
				doc.name = req.body.name;

				doc.save((err, doc) => {
					res.json(doc);
				});
			}
		});
	} else {
		res.status(400);
		res.send();
	}
});

restAPI.delete('/users/:id', (req, res) => {
	User.remove({
		_id: new mongodb.ObjectId(req.params.id)
	}, (err, docs) => {
		if (err) {
			throw err;
		} else {
			res.json(docs);
		}
	});
});

restAPI.get('/tasks/', (req, res) => {
	Task.find().sort('-date').exec((err, docs) => {
		if (err) {
			throw err;
		} else {
			res.json(docs);
		}
	});
});

restAPI.get('/tasks/:id', (req, res) => {
	Task.find({
		_id: new mongodb.ObjectId(req.params.id)
	}).exec((err, docs) => {
		if (err) {
			throw err;
		} else {
			res.json(docs);
		}
	});
});

restAPI.post('/tasks/', (req, res) => {
	if (req.body.name) {
		const newTask = new Task({
			name: req.body.name,
			description: req.body.description,
			user: req.body.user
		});

		newTask.save(err => {
			if (err) {
				res.json(err);
			} else {
				res.json(true);
			}
		});
	} else {
		res.status(400);
		res.send();
	}
});

restAPI.put('/tasks/:id', (req, res) => {
	Task.findOne({
		_id: new mongodb.ObjectId(req.params.id)
	}, (err, doc) => {
		if (err) {
			throw err;
		} else {
			doc.name = req.body.name;
			doc.description = req.body.description;
			doc.isOpened = (req.body.isOpened == 1) ? true : false;

			(req.body.isOpened == 0) ? doc.date = new Date() : '';

			doc.user = req.body.user;

			doc.save((err, doc) => {
				res.json(doc);
			});
		}
	});
});

restAPI.delete('/tasks/:id', (req, res) => {
	Task.remove({
		_id: new mongodb.ObjectId(req.params.id)
	}, (err, docs) => {
		if (err) {
			throw err;
		} else {
			res.json(docs);
		}
	});
});

restAPI.post('/search/', function(req, res) {
	Task.aggregate([{
		$match: {
			$and: [{
    		name: new RegExp(req.body.name, 'i')
    	}, {
    		description: new RegExp(req.body.description, 'i')
    	}]
   	}
  }], function(err, tasks) {
  	if (err) {
  		throw err;
  	} else {
  		res.json(tasks);
  	}
  });
});

app.use('/api/v1', restAPI);
app.listen(3000);