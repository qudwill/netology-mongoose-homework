var SERVER_BASE = 'http://localhost:3000';

var App = (function() {
	var $users = $('#users');
	var $tasks = $('#tasks');

	function setupListeners() {
		$('#add-user').on('submit', _addUser);
		$(document).on('click', '.edit-user', _showEditForm);
		$(document).on('submit', '.update-username', _updateUsername);
		$(document).on('click', '.remove-user', _removeUser);
		$('#add-task').on('submit', _addTask);
		$(document).on('click', '.edit-task', _showTaskEditForm);
		$(document).on('submit', '.update-task', _updateTask);
		$('.search-input').on('keyup', _search);
	}

	function _renderUsers() {
		$.ajax({
			url: SERVER_BASE + '/api/v1/users',
			type: 'get',
			success: function(data) {
				if (data) {
					var html = '';
					var options = '';

					$.each(data, function(key, item) {
						html += '<li>';
						html += '<span class="username">' + item.name + '</span> ';
						html += '<a href="#" class="edit-user" data-id="' + item._id + '">Редактировать</a> ';
						html += '<a href="#" class="remove-user" data-id="' + item._id + '">X</a>';
						html += '</li>';

						options += '<option value="' + item.name + '">' + item.name + '</option>';
					});

					$users.html(html);
					$('.user-options').html(options);
				}
			}
		});
	}

	function _addUser(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/users',
    	contentType: 'application/json',
			type: 'post',
			data: data,
			success: function(data) {
				if (data === true) {
					alert('Пользователь ' + formData.name + ' успешно добавлен.');
					_renderUsers();
				} else {
					alert(data.errmsg);
				}
			}
		});
	}

	function _getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function(n, i){
      indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
	}

	function _showEditForm(e) {
		e.preventDefault();

		var $this = $(this);
		var $li = $this.parent();
		var html = '';

		html += '<li>';
		html += '<form action="" class="update-username">';
		html += '<input type="text" name="name" value="' + $this.siblings('.username').html() + '">';
		html += '<input type="hidden" name="_id" value="' + $this.data('id') + '">';
		html += '<button type="submit">Обновить</button>';
		html += '</form>';
		html += '</li>';

		$li.after(html);
	}

	function _updateUsername(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/users/' + formData._id,
    	contentType: 'application/json',
			type: 'put',
			data: data,
			success: function(data) {
				if (data) {
					alert('Пользователь с _id ' + data._id + ' успешно переименован.');
					_renderUsers();
				}
			}
		});
	}

	function _removeUser(e) {
		e.preventDefault();

		$.ajax({
			url: SERVER_BASE + '/api/v1/users/' + $(this).data('id'),
			type: 'delete',
			success: function(data) {
				alert('Удаление пользователя прошло успешно');
				_renderUsers();

				console.log(data);
			}
		});
	}

	function _addTask(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/tasks',
    	contentType: 'application/json',
			type: 'post',
			data: data,
			success: function(data) {
				if (data === true) {
					alert('Задание ' + formData.name + ' успешно добавлено.');
					_renderTasks();
				} else {
					alert(data.errmsg);
				}
			}
		});
	}

	function _renderTasks() {
		$.ajax({
			url: SERVER_BASE + '/api/v1/tasks',
			type: 'get',
			success: function(data) {
				if (data) {
					_renderTasksHTML(data);
				}
			}
		});
	}

	function _updateUsername(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/users/' + formData._id,
    	contentType: 'application/json',
			type: 'put',
			data: data,
			success: function(data) {
				if (data) {
					alert('Пользователь с _id ' + data._id + ' успешно переименован.');
					_renderUsers();
				}
			}
		});
	}

	function _showTaskEditForm(e) {
		e.preventDefault();

		var $this = $(this);
		var _id = $this.data('id');

		$.ajax({
			url: SERVER_BASE + '/api/v1/tasks/' + _id,
			type: 'get',
			success: function(data) {
				if (data) {
					var html = '';
					var $tr = $this.parent().parent();

					$.each(data, function(key, item) {
						let opened = '';
						let finished = '';

						(item.isOpened) ? opened = ' selected=""' : finished = ' selected =""';

						html += '<tr>';
						html += '<td colspan="7">';
						html += '<form action="" class="update-task">';
						html += '<label for="name">Имя задачи</label>';
						html += '<br>';
						html += '<input type="text" name="name" value="' + item.name + '">';
						html += '<br>';
						html += '<label for="description">Описание задачи</label>';
						html += '<br>';
						html += '<textarea name="description" cols="30" rows="10" required="">' + item.description + '</textarea>';
						html += '<br>';
						html += '<select name="isOpened" required="">';
						html += '<option value="1"' + opened + '>Открыто</option>';
						html += '<option value="0"' + finished + '>Выполнено</option>';
						html += '</select>';
						html += '<br>';
						html += '<label for="user">Пользователь</label>';
						html += '<br>';
						html += '<select name="user" class="user-options" data-id="' + item._id + '" required="">';

						$.ajax({
							url: SERVER_BASE + '/api/v1/users',
							type: 'get',
							success: function(data) {
								if (data) {			
									var options = '';	

									$.each(data, function(key, user) {
										const selected = (item.user === user.name) ? ' selected=""' : '';

										options += '<option value="' + user.name + '"' + selected + '>' + user.name + '</option>';

									});

									$('.user-options[data-id="' + item._id + '"]').html(options);
								}
							}
						});

						html += '</select>';
						html += '<br>';
						html += '<input type="hidden" name="_id" value="' + $this.data('id') + '">';
						html += '<br>';
						html += '<button type="submit">Обновить</button>';
						html += '</form>';
						html += '</td>';
						html += '</tr>';
					});

					$tr.after(html);
				}
			}
		});
	}

	function _updateTask(e) {
		e.preventDefault();

		var formData = _getFormData($(this));
		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/tasks/' + formData._id,
    	contentType: 'application/json',
			type: 'put',
			data: data,
			success: function(data) {
				if (data) {
					alert('Задание с _id ' + data._id + ' успешно обновлено.');
					_renderTasks();
				}
			}
		});
	}

	function _search() {
		var searchByNameValue = $('#search-name').val();
		var searchByDescriptionValue = $('#search-description').val();

		var formData = {
			name: searchByNameValue,
			description: searchByDescriptionValue
		}

		var data = JSON.stringify(formData);

		$.ajax({
			url: SERVER_BASE + '/api/v1/search',
			type: 'post',
			contentType: 'application/json',
			data: data,
			success: function(data) {
				console.log(data);
				
				_renderTasksHTML(data);
			}
		});
	}

	function _renderTasksHTML(data) {
		var html = '';

		html += '<thead>';
		html += '<tr>';
		html += '<td>Имя</td>';
		html += '<td>Описание</td>';
		html += '<td>Состояние задачи</td>';
		html += '<td>Дата исполнения</td>';
		html += '<td>Пользователь</td>';
		html += '<td>Редактировать</td>';
		html += '<td>Удалить</td>';
		html += '</tr>';
		html += '</thead>';
		html += '<tbody>';

		$.each(data, function(key, item) {
			const isOpenedStatus = (item.isOpened) ? 'Открыто' : 'Выполнено';
			const date = (item.date) ? item.date : '';

			html += '<tr>';
			html += '<td>' + item.name + '</td>';
			html += '<td>' + item.description + '</td>';
			html += '<td>' + isOpenedStatus + '</td>';
			html += '<td>' + date + '</td>';
			html += '<td>' + item.user + '</td>';
			html += '<td><a href="#" class="edit-task" data-id="' + item._id + '">Редактировать</a></td>';
			html += '<td><a href="#" class="remove-task" data-id="' + item._id + '">X</a></td>';
			html += '</tr>';
		});

		html += '</tbody>';

		$tasks.html(html);
	}

	return {
		init: function() {
			setupListeners();
			_renderUsers();
			_renderTasks();
		}
	}
}());

$(document).ready(function() {
	App.init();
});